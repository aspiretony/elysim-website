<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Account Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Languages
 * @property \Cake\ORM\Association\HasMany $AccountBan
 * @property \Cake\ORM\Association\HasMany $AccountRewards
 * @property \Cake\ORM\Association\HasMany $AccountService
 *
 * @method \App\Model\Entity\Account get($primaryKey, $options = [])
 * @method \App\Model\Entity\Account newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Account[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Account|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Account[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Account findOrCreate($search, callable $callback = null)
 */
class AccountTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('account');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Languages', [
            'foreignKey' => 'language_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AccountBan', [
            'foreignKey' => 'account_id'
        ]);
        $this->hasMany('AccountRewards', [
            'foreignKey' => 'account_id'
        ]);
        $this->hasMany('AccountService', [
            'foreignKey' => 'account_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('account', 'create')
            ->notEmpty('account');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('pin', 'create')
            ->notEmpty('pin');

        $validator
            ->integer('cash')
            ->requirePresence('cash', 'create')
            ->allowEmpty('cash');

        $validator
            ->requirePresence('logged_in', 'create')
            ->allowEmpty('logged_in');

        $validator
            ->integer('access_level')
            ->requirePresence('access_level', 'create')
            ->notEmpty('access_level');

        $validator
            ->requirePresence('active', 'create')
            ->allowEmpty('active');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('location', 'create')
            ->allowEmpty('location');

        $validator
            ->requirePresence('date_created', 'create')
            ->allowEmpty('date_created');

        $validator
            ->requirePresence('date_last_login', 'create')
            ->allowEmpty('date_last_login');

        $validator
            ->requirePresence('creator_ip', 'create')
            ->allowEmpty('creator_ip');

        $validator
            ->requirePresence('last_ip', 'create')
            ->allowEmpty('last_ip');

        $validator
            ->requirePresence('current_ip', 'create')
            ->allowEmpty('current_ip');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
