<div class="account form large-9 medium-8 columns content">
    <?= $this->Form->create($account) ?>
    <fieldset>
        <legend><?= __('Add Account') ?></legend>
        <?php
            echo $this->Form->input('account');
            echo $this->Form->input('password');
            echo $this->Form->input('email');
            echo $this->Form->input('pin');
            echo $this->Form->input('cash');
            echo $this->Form->input('language_id');
            echo $this->Form->input('logged_in');
            echo $this->Form->input('access_level');
            echo $this->Form->input('active');
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('location');
            echo $this->Form->input('date_created');
            echo $this->Form->input('date_last_login');
            echo $this->Form->input('creator_ip');
            echo $this->Form->input('last_ip');
            echo $this->Form->input('current_ip');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
