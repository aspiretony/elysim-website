<section class="content-header">
    <h1>
       Gerenciamento de contas
        <small>Listagem das contas</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Listagem de todas as contas</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('account') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('cash') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('logged_in') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('access_level') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('location') ?></th>
                            <th scope="col" class="actions"><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($account as $account): ?>
                        <tr>
                            <td><?= $this->Number->format($account->id) ?></td>
                            <td><?= h($account->account) ?></td>
                            <td><?= h($account->password) ?></td>
                            <td><?= h($account->email) ?></td>
                            <td><?= $this->Number->format($account->cash) ?></td>
                            <td><?= h($account->logged_in) ?></td>
                            <td><?= $this->Number->format($account->access_level) ?></td>
                            <td><?= h($account->active) ?></td>
                            <td><?= h($account->first_name) ?></td>
                            <td><?= h($account->last_name) ?></td>
                            <td><?= h($account->location) ?></td>
                            <th   <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><?= $this->Html->link(__('Ver'), ['action' => 'visualizar', $account->id]) ?></li>
                                    <li> <?= $this->Html->link(__('Editar'), ['action' => 'editar', $account->id]) ?></li>
                                    <li><?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $account->id], ['confirm' => __('Are you sure you want to delete # {0}?', $account->id)]) ?></li>
                                </ul>
                            </div>
                            </th>
                        </tr>
                        </tfoot>
                        <?php endforeach; ?>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <p><?= $this->Paginator->counter() ?></p>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<?php
$this->Html->css([
    'AdminLTE./plugins/datatables/dataTables.bootstrap',
],
    ['block' => 'css']);

$this->Html->script([
    'AdminLTE./plugins/datatables/jquery.dataTables.min',
    'AdminLTE./plugins/datatables/dataTables.bootstrap.min',
],
    ['block' => 'script']);
?>

<?php $this->start('scriptBotton'); ?>
<script>
    $(function () {
        $("#example").DataTable();
        });
    });
</script>
<?php $this->end(); ?>