<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('custom.css') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('jquery-2.1.4.min.js') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">MapleStrap</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="?base=main">Home</a></li>
                <li><a href="?base=main&amp;page=download">Download</a></li>
                <li><a href="?base=main&amp;page=rankings">Rankings</a></li>
                <li><a href="?base=main&amp;page=vote">Vote</a></li>
                <li><a href="#">Forums</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:document.getElementById('sporter04').focus()">Login</a></li>
                <li><a href="?base=main&amp;page=register">Register</a></li>
            </ul>
        </div>
    </div>
</nav>
<header class="jumbotron gradient">
    <div class="container">
        <h1>MapleStrap</h1>
        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Control Panel</h3>
                </div>
                <div class="panel-body">
                    <form id="loginform">
                        <div class="form-group">
                            <input id="sporter04" class="form-control" placeholder="Username" type="text">
                        </div>
                        <div class="form-group">
                            <input id="sporter05" class="form-control" placeholder="Password" type="password">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-violet" type="submit">Login</button>
                        </div>
                    </form>
                    <div id="message"></div>
                </div>
            </div>
            <div id="servinfo" class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Server Info</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <p>Version:</p>
                            <p>Players:</p>
                            <p>Rates:</p>
                        </div>
                        <div class="col-xs-6 text-right">
                            <p><a href="?base=main&amp;page=download">62</a></p>
                            <p>256</p><!-- If server is offline it says Offline -->
                            <p>2, 1, 1</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Home</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="media welcome-block">
                            <div class="media-left">

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><?= $this->fetch('title') ?></h4>
                            </div>
                        </div>
                        <?= $this->fetch('content') ?>
                        <?= $this->Flash->render() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="col-lg-12">
            <ul class="list-unstyled list-inline">
                <li><a href="?base=main">Home</a></li>
                <li><a href="?base=main&amp;page=tos">Terms of Service</a></li>
                <li><a href="?base=main&amp;page=privacy">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="col-lg-12">
            <p>Copyright © 2015 MapleStrap. Proudly powered by <a href="https://forum.ragezone.com/f689/php-web-framework-maplestory-1087466/">Sporter Framework</a>. Template by <a href="http://forum.ragezone.com/members/1333372200.html">Snopboy</a>.<br>
                Bootstrap by Twitter Boostrap <a href="https://github.com/orgs/twbs/people">Team</a> &amp; MapleStory by <a href="http://www.nexon.net">NEXON</a> Corp.</p>
        </div>
    </div>
</footer>
<script src="static/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="static/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>