<section class="content-header">
    <h1>
     ID da conta: <?= h($account->id) ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center"><?= h($account->account) ?>    </h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Senha</b> <a class="pull-right"><?= h($account->password) ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="pull-right"><?= h($account->email) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>PIN</b> <a class="pull-right"><?= h($account->pin) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Cash</b> <a class="pull-right"><?= h($account->cash) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Linguagem</b> <a class="pull-right"><?= h($account->language_id) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Conectado</b> <a class="pull-right"><?= h($account->logged_in) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Nivel de acesso</b> <a class="pull-right"><?= h($account->access_level) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Atividade</b> <a class="pull-right"><?= h($account->active) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Nome</b> <a class="pull-right"><?= h($account->first_name) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Sobrenome</b> <a class="pull-right"><?= h($account->last_name) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Local</b> <a class="pull-right"><?= h($account->location) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Data criada</b> <a class="pull-right"><?= h($account->date_created) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Ultimo login</b> <a class="pull-right"><?= h($account->date_last_login) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>IP Criador</b> <a class="pull-right"><?= h($account->creator_ip) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>Ultimo IP</b> <a class="pull-right"><?= h($account->last_ip) ?> </a>
                        </li>
                        <li class="list-group-item">
                            <b>IP atual</b> <a class="pull-right"><?= h($account->current_ip) ?> </a>
                        </li>
                    </ul>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $account->id],['class' => 'btn btn-primary btn-danger'], ['confirm' => __('Are you sure you want to delete # {0}?', $account->id)]) ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
                    <li><a href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="timeline">
                        <!-- The timeline -->
                        g
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="settings">
                                <?= $this->Form->create($account, [
                                        'class' => 'form-horizontal'
                                ] ); ?>
                                <fieldset>
                                    <legend><?= __('Edit Account') ?></legend>
                                    <?php
                                    echo $this->Form->input('account',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'Conta'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('password',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'Senha'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('email',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'email'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('pin',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'pin'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('cash',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'cash'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('language_id',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'language_id'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('logged_in',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'logged_in'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('access_level',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'access_level'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('active',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'active'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('first_name',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'first_name'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('last_name',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'last_name'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('location',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'location'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('date_created',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'date_created'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('date_last_login',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'date_last_login'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('creator_ip',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'creator_ip'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('last_ip',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'last_ip'
                                        ]
                                    ]);
                                    ?>
                                    <?php
                                    echo $this->Form->input('current_ip',  [
                                        'label' => [
                                            'class' => 'col-sm-10',
                                            'text' => 'current_ip'
                                        ]
                                    ]);
                                    ?>
                                </fieldset>

                                <?= $this->Form->button(__('Submit')) ?>
                                <?= $this->Form->end() ?>


                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->










<div class="account view large-9 medium-8 columns content">
    <h3><?= h($account->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Account') ?></th>
            <td><?= h($account->account) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($account->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($account->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pin') ?></th>
            <td><?= h($account->pin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Language Id') ?></th>
            <td><?= h($account->language_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Logged In') ?></th>
            <td><?= h($account->logged_in) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= h($account->active) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($account->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($account->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Location') ?></th>
            <td><?= h($account->location) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Created') ?></th>
            <td><?= h($account->date_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Last Login') ?></th>
            <td><?= h($account->date_last_login) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creator Ip') ?></th>
            <td><?= h($account->creator_ip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Ip') ?></th>
            <td><?= h($account->last_ip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Current Ip') ?></th>
            <td><?= h($account->current_ip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($account->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cash') ?></th>
            <td><?= $this->Number->format($account->cash) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Access Level') ?></th>
            <td><?= $this->Number->format($account->access_level) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Account Ban') ?></h4>
        <?php if (!empty($account->account_ban)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Id') ?></th>
                <th scope="col"><?= __('Start Time') ?></th>
                <th scope="col"><?= __('End Time') ?></th>
                <th scope="col"><?= __('Reason') ?></th>
                <th scope="col"><?= __('Expire') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($account->account_ban as $accountBan): ?>
            <tr>
                <td><?= h($accountBan->id) ?></td>
                <td><?= h($accountBan->account_id) ?></td>
                <td><?= h($accountBan->start_time) ?></td>
                <td><?= h($accountBan->end_time) ?></td>
                <td><?= h($accountBan->reason) ?></td>
                <td><?= h($accountBan->expire) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AccountBan', 'action' => 'view', $accountBan->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'AccountBan', 'action' => 'edit', $accountBan->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AccountBan', 'action' => 'delete', $accountBan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountBan->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Account Rewards') ?></h4>
        <?php if (!empty($account->account_rewards)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Id') ?></th>
                <th scope="col"><?= __('Added') ?></th>
                <th scope="col"><?= __('Points') ?></th>
                <th scope="col"><?= __('Bonus Points') ?></th>
                <th scope="col"><?= __('Received') ?></th>
                <th scope="col"><?= __('Rewarded') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($account->account_rewards as $accountRewards): ?>
            <tr>
                <td><?= h($accountRewards->id) ?></td>
                <td><?= h($accountRewards->account_id) ?></td>
                <td><?= h($accountRewards->added) ?></td>
                <td><?= h($accountRewards->points) ?></td>
                <td><?= h($accountRewards->bonus_points) ?></td>
                <td><?= h($accountRewards->received) ?></td>
                <td><?= h($accountRewards->rewarded) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AccountRewards', 'action' => 'view', $accountRewards->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'AccountRewards', 'action' => 'edit', $accountRewards->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AccountRewards', 'action' => 'delete', $accountRewards->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountRewards->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Account Service') ?></h4>
        <?php if (!empty($account->account_service)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Id') ?></th>
                <th scope="col"><?= __('Service Id') ?></th>
                <th scope="col"><?= __('Start Time') ?></th>
                <th scope="col"><?= __('End Time') ?></th>
                <th scope="col"><?= __('Expire') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($account->account_service as $accountService): ?>
            <tr>
                <td><?= h($accountService->id) ?></td>
                <td><?= h($accountService->account_id) ?></td>
                <td><?= h($accountService->service_id) ?></td>
                <td><?= h($accountService->start_time) ?></td>
                <td><?= h($accountService->end_time) ?></td>
                <td><?= h($accountService->expire) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'AccountService', 'action' => 'view', $accountService->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'AccountService', 'action' => 'edit', $accountService->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'AccountService', 'action' => 'delete', $accountService->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountService->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
